import { ICarShop } from './../../shared/models/interfaces/car-shop';
import { Action, Store } from '@ngrx/store';
import { ICar } from '../../shared/models/interfaces/car';
import { AppState } from '../index';

export enum CarsTypes {
    LoadCars = '[CAR-SHOP] load cars [...]',
    LoadCarsSuccess = '[CAR-SHOP] load cars [SUCCESS]',
    LoadCarsError = '[CAR-SHOP] load cars [ERROR]',
    LoadAddCar = '[CAR-SHOP] add car [...]',
    LoadAddCarSuccess = '[CAR-SHOP] add car [SUCCESS]',
    LoadAddCarError = '[CAR-SHOP] add car [ERROR]',
    EditCar = '[CAR-SHOP] edit car [...]',
    EditCarSuccess = '[CAR-SHOP] edit car [SUCCESS]',
    EditCarError = '[CAR-SHOP] edit car [ERROR]',
    DeleteCar = '[CAR-SHOP] delete car [...]',
    DeleteCarSuccess = '[CAR-SHOP] delete car [SUCCESS]',
    DeleteCarError = '[CAR-SHOP] delete car [ERROR]',
}

///
export class LoadCarsAction implements Action {
    readonly type = CarsTypes.LoadCars;
}

export class LoadCarsSuccessAction implements Action {
    readonly type = CarsTypes.LoadCarsSuccess;
    constructor(public payload: ICar[]) { }
}

export class LoadCarsErrorAction implements Action {
    readonly type = CarsTypes.LoadCarsError;
    constructor(public payload: any) { }
}

/// 

export class LoadAddCarAction implements Action {
    readonly type = CarsTypes.LoadAddCar;
    constructor(public payload: { shopId: string, car: ICar }) { }
    // constructor(public shopId, public formValue) { }
}

export class LoadAddCarSuccessAction implements Action {
    readonly type = CarsTypes.LoadAddCarSuccess;
    constructor(public payload: ICar) { }
}

export class LoadAddCarErrorAction implements Action {
    readonly type = CarsTypes.LoadAddCarError;
    constructor(public payload: any) { }
}

///

export class EditCarAction implements Action {
    readonly type = CarsTypes.EditCar;
    constructor(public payload: ICar) { }
    // constructor(public shopId, public formValue) { }
}

export class EditCarSuccessAction implements Action {
    readonly type = CarsTypes.EditCarSuccess;
    constructor(public payload: ICar) { }
}

export class EditCarErrorAction implements Action {
    readonly type = CarsTypes.EditCarError;
    constructor(public payload: any) { }
}

// экшн на удаление машины из стора

export class DeleteCarAction implements Action {
    readonly type = CarsTypes.DeleteCar;
    constructor(public payload: string) { }
}

export class DeleteCarSuccessAction implements Action {
    readonly type = CarsTypes.DeleteCarSuccess;
    constructor(public payload: string) { }
}

export class DeleteCarErrorAction implements Action {
    readonly type = CarsTypes.DeleteCarError;
    constructor(public payload: any) { }
}





// // экшн на удаление шопа из стора
// export class DeleteShopAction implements Action {
//     // объявление свойства type у экшена, по нему редюсер определит свое поведение
//     readonly type = CarShopsTypes.DeleteShop;
//     // в конструктор передаются необходимые значения
//     constructor(public shopId: number) { }
// }

// export class EditCarAction implements Action {
//     // объявление свойства type у экшена, по нему редюсер определит свое поведение
//     readonly type = CarShopsTypes.EditCar;
//     // в конструктор передаются необходимые значения (id магазина и машины, плюс объект
//     // с новыми значениями из формы)
//     constructor(public shopId: number, public carId: number, public formValue: ICar) {
//     }
// }

// // экшн на удаление машины из стора
// export class DeleteCarAction implements Action {
//     // объявление свойства type у экшена, по нему редюсер определит свое поведение
//     readonly type = CarShopsTypes.DeleteCar;
//     // в конструктор передаются необходимые значения
//     constructor(public shopId: number, public carId: number) {
//     }
// }

// тут экспортируются типы
export type CarsActions
    = LoadCarsAction
    | LoadCarsSuccessAction
    | LoadCarsErrorAction

    | LoadAddCarAction
    | LoadAddCarSuccessAction
    | LoadAddCarErrorAction

    | EditCarAction
    | EditCarSuccessAction
    | EditCarErrorAction

    | DeleteCarAction
    | DeleteCarSuccessAction
    | DeleteCarErrorAction
    ;



