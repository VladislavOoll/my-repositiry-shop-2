import { HttpService } from './../../shared/services/http.service';
import { switchMap, map, tap, catchError } from 'rxjs/operators';
import { of, Observable, empty } from 'rxjs';
import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import { CarsActions, LoadCarsAction, LoadCarsSuccessAction, LoadCarsErrorAction, CarsTypes, LoadAddCarAction, LoadAddCarErrorAction,
     LoadAddCarSuccessAction, EditCarAction, EditCarSuccessAction, EditCarErrorAction, DeleteCarAction, DeleteCarSuccessAction, DeleteCarErrorAction } from './cars.action';
import { AddCarToShopAction, DeleteCarFromShopAction } from '../car-shops/car-shops.action';
import { Action } from '@ngrx/store';

@Injectable()
export class CarsEffects {
    @Effect()
    loadCars$: Observable<CarsActions> = this.actions$.pipe(
        ofType<LoadCarsAction>(CarsTypes.LoadCars),
        switchMap(() => {
            return this.http.getCars().pipe(
                map((res: any) => new LoadCarsSuccessAction(res)),
                catchError(error => of(new LoadCarsErrorAction(error))),
            );
        }),
    );

    // @Effect({ dispatch: false })
    // onError$: Observable<CarsActions> = this.actions$.pipe(
    //     ofType<LoadCarsErrorAction>
    //         (CarsTypes.LoadCarsError),
    //     map(action => action.payload),
    //     tap(e => {
    //         console.error(e);
    //         // this.errorService.toConsole(e);
    //     }),
    // );

    @Effect()
    addCar$: Observable<CarsActions> = this.actions$.pipe(
        ofType<LoadAddCarAction>(CarsTypes.LoadAddCar),
        switchMap((value) => {
            console.log('vaaalue', value);
            return this.http.addCar(value.payload.shopId, value.payload.car).pipe(
                switchMap((res: any) => {
                    return of(new LoadAddCarSuccessAction({  ...value.payload.car, _id: res  }),
                        new AddCarToShopAction({ shopId: value.payload.shopId, carId: res }));
                }),
                catchError(error => of(new LoadAddCarErrorAction(error))),
            );
        }),
    );

    @Effect({ dispatch: false })
    onAddError$: Observable<CarsActions> = this.actions$.pipe(
        ofType<LoadAddCarErrorAction>
            (CarsTypes.LoadAddCarError),
        map(action => action.payload),
        tap(e => {
            console.error(e);
            // this.errorService.toConsole(e);
        }),
    );

    @Effect()
    EditCar$: Observable<CarsActions> = this.actions$.pipe(
        ofType<EditCarAction>(CarsTypes.EditCar),
        switchMap((value) => {
            return this.http.editCar(value.payload).pipe(
                map(() => {
                    return new EditCarSuccessAction({  ...value.payload  })
                }),
                catchError(error => of(new EditCarErrorAction(error))),
            );
        }),
    );

    @Effect({ dispatch: false })
    onEditError$: Observable<CarsActions> = this.actions$.pipe(
        ofType<EditCarErrorAction>
            (CarsTypes.EditCarError),
        map(action => action.payload),
        tap(e => {
            console.error(e);
            // this.errorService.toConsole(e);
        }),
    );

    @Effect()
    deleteCar$: Observable<CarsActions> = this.actions$.pipe(
        ofType<DeleteCarAction>(CarsTypes.DeleteCar),
        map(action => action.payload),
        switchMap((carId: string) => {
            return this.http.deleteCar(carId).pipe(
                switchMap(() => {
                    return of(
                        new DeleteCarSuccessAction(carId),
                        new DeleteCarFromShopAction(carId));
                }),
                catchError(error => of(new LoadAddCarErrorAction(error))),
            );
        }),
    );

    @Effect({ dispatch: false })
    onError$: Observable<Action> = this.actions$.pipe(
        ofType<DeleteCarErrorAction | EditCarErrorAction | LoadAddCarErrorAction | LoadCarsErrorAction>
            (CarsTypes.DeleteCarError, CarsTypes.EditCarError, CarsTypes.LoadAddCarError, CarsTypes.LoadCarsError),
        map(action => action.payload),
        tap(e => {
            console.error(e);
            // this.errorService.toConsole(e);
        }),
    );

    constructor(
        private actions$: Actions,
        private http: HttpService,
    ) { }
}

    // @Effect({ dispatch: false })
    // onDeleteError$: Observable<CarsActions> = this.actions$.pipe(
    //     ofType<DeleteCarErrorAction>
    //         (CarsTypes.DeleteCarError),
    //     map(action => action.payload),
    //     tap(e => {
    //         console.error(e);
    //         // this.errorService.toConsole(e);
    //     }),
    // );