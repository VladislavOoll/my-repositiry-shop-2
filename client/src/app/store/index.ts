import { ActionReducer } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';
import { MetaReducer } from '@ngrx/store';
import { ActionReducerMap } from '@ngrx/store';
import * as carShops from './car-shops/car-shops.reducer';
import { environment } from '../../environments/environment';
import * as cars from './cars/cars.reducer';



export interface AppState {
    carShops: carShops.CarShopsState;
    cars: cars.CarsState;
}

export const appReducers: ActionReducerMap<AppState> = {
    carShops: carShops.reducer,
    cars: cars.reducer,
};

export function logger(reducer: ActionReducer<AppState>): ActionReducer<AppState> {
	return function (state: AppState, action: any): AppState {
		console.log(state, action);
		return reducer(state, action);
	};
}

export const metaReducers: MetaReducer<AppState>[] = !environment.production
	? [logger, storeFreeze]
	: [];
