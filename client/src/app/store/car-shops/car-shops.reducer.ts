import { ICarShop } from './../../shared/models/interfaces/car-shop';
import { CarShopsActions, CarShopsTypes } from './car-shops.action';
import { createFeatureSelector } from '@ngrx/store';
import { ICar } from '../../shared/models/interfaces/car';

export class CarShopsState {
    shops: ICarShop[];
    // cars: ICar[];
    isCarShopsLoading: boolean = true;
    // isCarsLoading: boolean = true;
}


export function reducer(state = new CarShopsState(), action: CarShopsActions): CarShopsState {
    switch (action.type) {
        // LOAD CAR SHOPS
        case CarShopsTypes.LoadCarShops:
            return {
                ...state,
                isCarShopsLoading: true,
            };
        case CarShopsTypes.LoadCarShopsSuccess:
            return {
                ...state,
                shops: action.payload,
                isCarShopsLoading: false,
            };
        case CarShopsTypes.LoadCarShopsError:
            return {
                ...state,
                isCarShopsLoading: false,
                shops: [],
            };


        case CarShopsTypes.AddCarToShop:
            return {
                ...state,
                shops: [...state.shops.map((shop) => {
                    if (shop._id == action.payload.shopId) {
                        return { ...shop, cars: shop.cars.concat(action.payload.carId) }
                    } else return shop;
                })]
            }


        // case CarShopsTypes.AddCarToShop:
        //     return {
        //         ...state,
        //         shops: [...state.shops.map((shop) => {
        //             if (shop._id == action.payload.shopId) {
        //                 return { ...shop, cars: shop.cars.concat(action.payload.carId) }
        //             } else return shop;
        //         })]
        //     }

        // case CarShopsTypes.DeleteCarFromShop:
        //     return {
        //         ...state,
        //         shops: state.shops.map(shop => {
        //             return {
        //                 ...shop,
        //                 cars: shop.cars.filter(carId => {
        //                     return carId != action.payload;
        //                 })
        //             }
        //         }),
        //     }





        // // в случае если значение типа экшена (action.type) совпадает, выполняется объявленное в {}
        // case CarShopsTypes.DeleteShop: {
        //     // console.log(action, state);
        //     //создание константы для обновленного store.shops // отфильтровывается шоп с id переданным в экшен
        //     const updatedShops: ICarShop[] = state.shops.filter(shop => {
        //         return shop._id !== action.shopId
        //     })
        //     // console.log(updatedShops);
        //     //возвращается новый объект стора
        //     return {
        //         ...state,
        //         shops: updatedShops,
        //     };
        // }
        // // удаление одной машины из шопа
        // case CarShopsTypes.DeleteCar: {
        //     // через оператор map перебераются объекты из массива state.shops 
        //     const updatedShops: ICarShop[] = state.shops.map(shop => {
        //         // если id шопа не совпадает со свойством shopId переданным в action, 
        //         // этот шоп возвращается без изменений.
        //         if (shop._id != action.shopId) return shop;
        //         // иначе через оператор filter перебираются все объекты из массива shop.cars,
        //         // и отфильтровывается объект из массива со свойством _id равным action.carId
        //         const updatedShopCars: ICar[] = shop.cars.filter(car => car._id != action.carId)
        //         // далее возвращаем объект, в котором распространяем через оператор ... 
        //         // все значения перебираемого через MAP шопа, и дополнительно перезаписываем
        //         // имеющееся в нем свойство cars, на свойство cars со значением отфильтрованного
        //         // строкой выше массива
        //         return {
        //             ...shop,
        //             cars: updatedShopCars
        //         };
        //     })
        //     return {
        //         ...state,
        //         shops: updatedShops,
        //     };
        // }
        // case CarShopsTypes.EditCar: {
        //     // через оператор map перебераются объекты из массива state.shops 
        //     const updatedShops: ICarShop[] = state.shops.map(shop => {
        //         // если id шопа не совпадает со свойством shopId переданным в action, 
        //         // этот шоп возвращается без изменений.
        //         if (shop._id != action.shopId) return shop;
        //         // иначе через оператор map перебираются все объекты из массива shop.cars,
        //         const updatedShopCars: ICar[] = shop.cars.map(
        //             car => {
        //                 //каждая машина проверяется на совпадение со значением из action.carId.
        //                 // если значения разные, то автомобиль возвращается без изменений
        //                 if (car._id != action.carId) return car;
        //                 // иначе возвращается объект нового объекта вида ICar
        //                 // свойство _id берется из исходного объекта,
        //                 // а остальные свойства берутся из переданных значений формы
        //                 return {
        //                     _id: car._id,
        //                     ...action.formValue
        //                 }
        //             }
        //         )
        //         // далее возвращаем объект, в котором распространяем через оператор ... 
        //         // все значения перебираемого через MAP шопа, и дополнительно перезаписываем
        //         // имеющееся в нем свойство cars, на свойство cars со значением отфильтрованного
        //         // строкой выше массива
        //         return {
        //             ...shop,
        //             cars: updatedShopCars
        //         };
        //     })
        // return {
        //     ...state,
        //     shops: updatedShops,
        // };
        // }
        // case CarShopsTypes.AddCar: {
        //     // через оператор map перебераются объекты из массива state.shops 
        //     const updatedShops: ICarShop[] = state.shops.map(shop => {
        //         // если id шопа не совпадает со свойством shopId переданным в action, 
        //         // этот шоп возвращается без изменений.
        //         if (shop._id != action.shopId) return shop;
        //         // иначе через оператор concat в массив shop.cars
        //         // добавляются элементы из нового массива,
        //         // со значениями из формы и свойством _id,
        //         // равным длинне массива.
        //         // _id сделаем равным случайному числу из даты в миллисекундах
        //         const idFromDate = new Date().getTime();
        //         const updatedShopCars: ICar[] = shop.cars.concat(
        //             [{...action.formValue, _id: +idFromDate}]
        //         )
        //         // далее возвращаем объект, в котором распространяем через оператор ... 
        //         // все значения перебираемого через MAP шопа, и дополнительно перезаписываем
        //         // имеющееся в нем свойство cars, на свойство cars со значением отфильтрованного
        //         // строкой выше массива
        //         return {
        //             ...shop,
        //             cars: updatedShopCars
        //         };
        //     })
        //     console.log(state)
        //     return {
        //         ...state,
        //         shops: updatedShops,
        //     };
        // }
        default: {
            return state;
        }
    }
}

export const getCarShopsStoreState = createFeatureSelector<CarShopsState>('carShops');