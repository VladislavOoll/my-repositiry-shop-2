import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../store';
import { map, withLatestFrom, switchMap } from 'rxjs/operators';
import { getCarShopsStoreState } from './../../store/car-shops/car-shops.reducer';
import { ICarShop } from '../../shared/models/interfaces/car-shop';
import { Observable, Subscription, combineLatest, empty, of } from 'rxjs';
import { ICar } from '../../shared/models/interfaces/car';
import { getCarsStoreState } from '../../store/cars/cars.reducer';
import { DeleteCarAction } from '../../store/cars/cars.action';

@Component({
    selector: 'car-test-shop',
    templateUrl: './shop.component.html',
    styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit, OnDestroy {
    shopId;
    carShop$: Observable<ICar[]>;
    carIds$: Observable<string[]>;
    sub: Subscription;

    // combineLatest для шопов и машин
    shop$: Observable<ICarShop>;
    cars$: Observable<ICar[]>;

    constructor(
        private activatedRoute: ActivatedRoute,
        private store: Store<AppState>,
        private router: Router,
    ) { }

    ngOnInit() {

        // про метод combineLatest() надо почитать это из RxJs он типа мержит две обсёрбл в одну сущность
        // получаем tupple - объект с разными типами. в нашем случаи ICar и ICarShops
        // на выходе тоже обсёрбл
        this.shop$ = combineLatest(
            // берём значение нашего урла из роут параметра, отправляем в пайп
            this.activatedRoute.params.pipe(map(p => {
                this.shopId = p.id;
                return p.id
            }
            )),
            // из стора достём shops
            this.store.select(getCarShopsStoreState).pipe(map(
                s => s.shops)),
        ).pipe(
            map(([id, shops]) => {
                if (!id || !shops || !shops.length) return;
                // На сколько я понимаю, оно сопоставляет id в урле с id шопов и возвращает подходящий
                return shops.find(s => s._id === id);
            }),
        );

        this.cars$ = combineLatest(
            // берём айди шопа
            this.shop$,
            // вытаскием из стора массив тачек
            this.store.select(getCarsStoreState).pipe(map(s => s.cars)),
        ).pipe(
            map(([shop, cars]) => {
                if (!shop || !cars || !shop.cars || !shop.cars.length || !cars.length) return;
                // фильтруем тачки по айди шопа
                return cars.filter(c => shop.cars.includes(c._id));
            }),
        )
    }

    onClick(i: number): void {
        this.router.navigate([`/car-info/${this.shopId}/${i}`]);
    }

    onAddClick(): void {
        this.router.navigate([`/edit/${this.shopId}`]);
    }

    onEditClick(i) {
        this.router.navigate([`/edit/${this.shopId}/${i}`]);
    }

    // onDeleteClick(carId: number) {
    //     // this.dataService.deleteCar(this.shopId, carId);
    // }

    onDeleteClick(carId: string): void {
        // event.stopPropogation();
        this.store.dispatch(new DeleteCarAction(carId));
    }

    goHome(): void {
        this.router.navigate([`/home`]);
    }

    ngOnDestroy() {
        if (this.sub) this.sub.unsubscribe;
    }

    // ngOnDestroy(): void {
    //     throw new Error("Method not implemented.");
    // }
}

