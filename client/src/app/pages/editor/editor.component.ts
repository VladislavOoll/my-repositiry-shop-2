import { Component, OnInit, OnDestroy } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/index';
import { getCarShopsStoreState } from '../../store/car-shops/car-shops.reducer';
import { ICar } from '../../shared/models/interfaces/car';
import { map } from 'rxjs/operators';
import { DataService } from '../../shared/services/data.service';
import { LoadAddCarAction, EditCarAction } from '../../store/cars/cars.action'; //, loadEditCarAction
import { getCarsStoreState } from '../../store/cars/cars.reducer';

@Component({
    selector: 'car-test-editor',
    templateUrl: './editor.component.html',
    styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit, OnDestroy {

    editorMode: boolean = false;
    carForm: FormGroup;
    subscription: Subscription;
    data$: Observable<ICar>;
    carId: string;
    shopId: string;

    constructor(
        private fb: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private store: Store<AppState>,
        private dataService: DataService
    ) { }

    ngOnInit(): void {
        // подписка на параметры передаваемые в url
        this.subscription = this.activatedRoute.params.subscribe((p) => {
            console.log(',,,', p);
            
            this.shopId = p.shopId;
            // если параметры переданы, то включается режим редактирования, которые изменяет название формы
            if (p.shopId && p.carId) {
                this.carId = p.carId;
                this.editorMode = true;
                this.data$ = this.store.select(getCarsStoreState).pipe(
                    map(s => {
                        // if (!s.cars) return;
                        // const shop = s.shops.find(s => s._id == p.shopId);
                        const car = s.cars && s.cars.find(c => c._id == p.carId);
                        // инициализируется форма со значениями из наденного выше объекта
                this.initForm(car);
                        return car;
                    }));
            } else {
                this.initForm();
            }
            // в переменную data$ записываются данные из store

        });
    }

    initForm(car?: ICar): void {
        if (car) {
            this.carForm = this.fb.group({
                img: [car.img, [Validators.required]],
                color: [car.color, [Validators.required]],
                model: [car.model, [Validators.required]],
                price: [car.price, [Validators.required]],
                brand: [car.brand, [Validators.required]],
                age: [car.age, [Validators.required]],
                description: [car.description, [Validators.required]],
            });
        } else {
            this.carForm = this.fb.group({
                img: ["https://cdn.img.inosmi.ru/images/24308/06/243080692.jpg", [Validators.required]],
                color: ["546456", [Validators.required]],
                model: ["567657", [Validators.required]],
                price: ["56757", [Validators.required]],
                brand: ["345345", [Validators.required]],
                age: ["345345", [Validators.required]],
                description: ["456456", [Validators.required]],
            })
        }
    }

    onSubmit(): void {
        if (!this.carForm.valid) return;
        if (this.editorMode) {
            this.editCar(this.carForm.value);
        } else {
            this.addCar(this.carForm.value);
        }
        this.router.navigate(['/home']);
    }

    addCar(formValue): void {
        console.log(this.shopId);
        
        this.store.dispatch(new LoadAddCarAction(
            {
                shopId: this.shopId,
                car: formValue
            }
        ));
    }

    // editCar(formValue): void {
    //     const routeParams = this.activatedRoute.params['value'];
    //     // передаем в сервис значения из роутера и объект значений из формы
    //     // this.dataService.editCar(routeParams.shopId, routeParams.carId, formValue)
    // }

    editCar(formValue): void {  // на обновление
        // const routeParams = this.activatedRoute.params['value'];
        // передаем в сервис значения из роутера и объект значений из формы
        // this.dataService.editCar(routeParams.shopId, routeParams.carId, formValue)
        console.log('editCar', this.carId, formValue)
        this.store.dispatch(new EditCarAction(
            {
                ...formValue,
                _id: this.carId
            }
        ))
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

}