import { ICarShop } from './../models/interfaces/car-shop';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ICar } from '../models/interfaces/car';
import { LoadCarShopsAction, AddCarToShopAction } from '../../store/car-shops/car-shops.action';
import { LoadCarsAction } from '../../store/cars/cars.action';
import { Store } from '@ngrx/store';
import { AppState } from '../../store';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class HttpService {
    constructor(private http: HttpClient, private store: Store<AppState>) { }

    get apiUrl(): string {
        return 'http://localhost:3000/api';
    }

    getCarShops(): Observable<ICarShop[]> {
        return this.http.get<ICarShop[]>(`${this.apiUrl}/shops`);
    }
    getCars(): Observable<ICar[]> {
        return this.http.get<ICar[]>(`${this.apiUrl}/cars`);
    }


    addCar(shopId: string, formValue: ICar): any {
        const body = { car: { ...formValue }, "shopId": shopId };
        console.log(body);
        
        return this.http.post<any>(`${this.apiUrl}/cars`, body)
        // .pipe(
            // map(resp=>{
            //     this.store.dispatch(AddCarToShopAction)
            // })
        // )
        // console.log(x);
        // return x
    }
    
    editCar(car: ICar): any {
        return this.http.put<any>(`${this.apiUrl}/cars`, car)
    }

    deleteCar(carId: string): any {
        return this.http.delete<any>(`${this.apiUrl}/cars/${carId}`);

    }

}

