import { Collections } from '../models/enums/collections';
import { ObjectId } from 'mongodb';
import { MongoService } from 'typespring';
import { Repository } from 'typespring';
import { ICar } from '../models/interfaces/car';
import assert = require('assert');

@Repository()
export class CarsRepository {

    constructor(
        private mongo: MongoService,
    ) { }

    async getCars(): Promise<ICar[]> {
        return this.mongo.collection(Collections.Cars).find({}).toArray();
    }

    async addCar(car, shopId): Promise<string> {
        const newCar = await this.mongo.collection(Collections.Cars).insertOne(car);
        assert.equal(newCar.insertedCount, 1, 'addCar insert error');
        console.log(car, shopId);
        const update = await this.mongo.collection(Collections.Shops)
            .updateOne(
                { _id: new ObjectId(shopId) },
                // оно из строки spopId делает ObjectId
                { $addToSet: { cars: newCar.insertedId } },
                // оператор эдтусэт используется для работы с массивами
                // в шопах у нас есть массивы с тачками cars,
                // мы туда записываем ObjectId новой тачки для добавления в шоп
            );
        assert.equal(update.modifiedCount, 1, 'addCar update error');

        return JSON.stringify(`${newCar.insertedId}`);

    }

    // async editCar(car, carId): Promise<void> {
    //     const id = new ObjectId(carId);
    //     delete car._id;
    //     await this.mongo.collection(Collections.Cars).replaceOne(
    //         { _id: id },
    //         { ...car },
    //     );
    // }

    async editCar(car): Promise<any> {
        console.log('car' , car);
        const carId = new ObjectId(car._id);
        delete car._id;
        const x = await this.mongo.collection(Collections.Cars).replaceOne(
            { _id: carId },
            { ...car },
        );
        assert.equal(x.modifiedCount, 1, 'editCar update error');
        console.log(x.result);
        return x.result;
    }

    async deleteCar(carId): Promise<void> {
        const id = new ObjectId(carId);
        await this.mongo.collection(Collections.Cars).deleteOne({ _id: id });
        await this.mongo.collection(Collections.Shops).updateOne(
            { cars: { $all: [id] }},
            { $pull: { cars: id } },
        );
    }

    // async deleteCar(carId, shopId): Promise<void> {
    //     const id = new ObjectId(carId);
    //     await this.mongo.collection(Collections.Cars).deleteOne({ _id: id });
    //     // метод делете,
    //     // её параметр это условие выборки по которой оно проверяет и при соотвествии выполнгяется
    //     await this.mongo.collection(Collections.Shops).updateMany(
    //         { _id: new ObjectId(shopId) },
    //         { $pull: { cars: id } },
    //         // метод пул для работы с массивами, а точнее для
    //         // удаления из массива данные, которые подходят по результатам выборки выше
    //         // читал, я думал это не из монги), тут везде доллары
    //         // тепер ья понял, что это монга и я это уже видел и что она так по коду используется
    //     );
    // }

    // async deleteCar(carId): Promise<void> {
    //     const id = new ObjectId(carId);
    //     console.log(id);
    //     console.log(await this.mongo.collection(Collections.Cars).findOne({_id: id}));
    //     const res = await this.mongo.collection(Collections.Cars).deleteOne({ _id: id });

    //     await this.mongo.collection(Collections.Shops).updateOne(
    //         { cars: { $all: [id] }},
    //         { $pull: { cars: id } },
    //     );
    // }
}