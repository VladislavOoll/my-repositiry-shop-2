import { Request } from 'express';
import { Controller, GetRequest, LoggerFactory, PostRequest, PutRequest, DeleteRequest } from 'typespring';
import { Logger } from 'winston';
// import { ICarShop } from '../models/interfaces/car-shop';
// import { carShops } from '../models/constants/cars.const';

import { CarsRepository } from '../repository/cars';
import { ICar } from '../models/interfaces/car';

@Controller('/cars')
export class CarsController {
    private _logger: Logger;

    constructor(
        loggerFactory: LoggerFactory,
        private carsRepo: CarsRepository,
    ) {
        this._logger = loggerFactory.getLogger('TEST');
    }

    @GetRequest('/')

    async getCars(req: Request): Promise<ICar[]> {
        this._logger.info('Requesting cars'); // будет работать и без этйо строчки
        return await this.carsRepo.getCars();
    }
    @PostRequest('/')
    // это асинхронная функция, эвейт её тормозит, то есть она
    // не выполниться пока не будет this.carsRepo.addCar(req.body.car, req.body.shopId)
    // оно в ф-ию addCar которая имеется в репозитории передаст два параметра
    async addCar(req: Request): Promise<String> {
        return await this.carsRepo.addCar(req.body.car, req.body.shopId);
    }

    // @PutRequest('/')

    // async editCar(req: Request): Promise<void> {
    //     return await this.carsRepo.editCar(req.body, req.body.carId);
    // }

    @PutRequest('/')
    // async addCar(req: Request): Promise<ICar> {
    async editCar(req: Request): Promise<void> {
        // console.log('post ctrl', req.body);
        return await this.carsRepo.editCar(req.body);
    }

    // @DeleteRequest('/:id')

    // async deleteCar(req: Request): Promise<void> {
    //     this._logger.info('Car removal'); // будет работать и без этйо строчки
    //     return await this.carsRepo.deleteCar(req.params.id);
    // }

    @DeleteRequest('/:id')
    async deleteCar(req: Request): Promise<void> {
        return await this.carsRepo.deleteCar(req.params.id);
    }
}